/**************************************************************************
*
* Copyright 2018-2021 ZD Rpety - DAKEL, Czech Republic
*
***************************************************************************
* This program is a property of Dakel.cz. You may use it after receiving
* written permission from dev@dakel.cz.
***************************************************************************
*
* zedo-rpc test module
*
***************************************************************************/

const zedorpc = require('./index.js');
const assert = require('assert');

let z = new zedorpc.ZedoRPC()

// use when debugging only
if(0)
{
    describe = function(name, func) { console.log(name); func(); }
    it = describe;
}

describe("Testing the ZedoRPC Object", function () {
    it("Testing compare_versions()", function() {
        assert(z.compare_versions("1.0.0.0", "2.0.0.0") == -1);
        assert(z.compare_versions("1.3.0.0", "2.0.0.0") == -1);
        assert(z.compare_versions("1.3.5.0", "2.0.0.0") == -1);
        assert(z.compare_versions("1.3.5.7", "2.0.0.0") == -1);
        assert(z.compare_versions("3.0.0.100", "4.0.0.99") == -1);
        assert(z.compare_versions("1.2.0.100", "1.2.0.101") == -1);
        assert(z.compare_versions("1.2.0.0", "1.2.0.101") == -1);

        assert(z.compare_versions("1.0.0.0", "1.0.0.0") == 0);
        assert(z.compare_versions("1.0.0.0", "1.0.0") == 0);
        assert(z.compare_versions("1.0.0.0", "1.0") == 0);
        assert(z.compare_versions("1.0.0", "1.0.0.0") == 0);
        assert(z.compare_versions("1.0.0", "1.0.0") == 0);
        assert(z.compare_versions("1.0.0", "1.0") == 0);
        assert(z.compare_versions("1.0", "1.0.0.0") == 0);
        assert(z.compare_versions("1.0", "1.0.0") == 0);
        assert(z.compare_versions("1.0", "1.0") == 0);

        assert(z.compare_versions("123", "123") == 0);
        assert(z.compare_versions("123", "1.0.0.123") == 0);
        assert(z.compare_versions("123", "1.3.5.123") == 0);
        assert(z.compare_versions("123", "2.3.8.123") == 0);
        assert(z.compare_versions("123", "0.0.0.123") == 0);

        assert(z.compare_versions("123", "123") == 0);
        assert(z.compare_versions(123, "123") == 0);
        assert(z.compare_versions("123", 123) == 0);
        assert(z.compare_versions("1.0.0.123", "123") == 0);
        assert(z.compare_versions("1.3.5.123", "123") == 0);
        assert(z.compare_versions("2.3.8.123", "123") == 0);
        assert(z.compare_versions("0.0.0.123", "123") == 0);

        assert(z.compare_versions("150", "123") == 1);
        assert(z.compare_versions(150, "123") == 1);
        assert(z.compare_versions("150", 123) == 1);
        assert(z.compare_versions("1.0.0.150", "123") == 1);
        assert(z.compare_versions("1.3.5.150", "123") == 1);
        assert(z.compare_versions("2.3.8.150", "123") == 1);
        assert(z.compare_versions("0.0.0.150", "123") == 1);

        assert(z.compare_versions("123", "150") == -1);
        assert(z.compare_versions(123, "150") == -1);
        assert(z.compare_versions("123", 150) == -1);
        assert(z.compare_versions("123", "1.0.0.150") == -1);
        assert(z.compare_versions("123", "1.3.5.150") == -1);
        assert(z.compare_versions("123", "2.3.8.150") == -1);
        assert(z.compare_versions("123", "0.0.0.150") == -1);
    });

    it("Testing is_json_object()", function() {
        let uninitialized;
        let initialized = 1;

        assert(z.is_json_object( { a: 1 } ));
        assert(z.is_json_object( { a: { b: 1 } } ));
        assert(z.is_json_object( { a: null } ));
        assert(z.is_json_object( { a: [ 1, 2 ] } ));
        assert(z.is_json_object( [ 3, 4 ] ));
        assert(z.is_json_object( { initialized } ));
        assert(z.is_json_object( { uninitialized } ));

        assert(!z.is_json_object( 0 ));
        assert(!z.is_json_object( 1 ));
        assert(!z.is_json_object( 0.1 ));
        assert(!z.is_json_object( 1e-4 ));
        assert(!z.is_json_object( null ));
        assert(!z.is_json_object( initialized ));
        assert(!z.is_json_object( uninitialized ));
        assert(!z.is_json_object( "one" ));
    });

    it("Testing copy_json()", function() {
        let uninitialized;
        let initialized = 1;
        let sub = { sub1: 1, sub2: { sub3: 3, sub4: 4 }, sub5: uninitialized, sub6: initialized };
        let src1 = { a:1, b:2, c: { d:1, e:[2,3,4], f:"test", e:null }, g:sub, h:[sub,sub] };

        same = function(o1, o2) { return JSON.stringify(o1).localeCompare(JSON.stringify(o2)) == 0; }

        let new1 = z.copy_json(src1);
        assert(same(new1, src1));

        assert(same(new1, src1));
        assert(new1 != src1)

        assert(same(new1.c, src1.c));
        assert(new1.c != src1.c)

        assert(same(new1.c.e, src1.c.e));
        //assert(new1.c.e != src1.c.e) // litral arrays may match!

        assert(same(new1.h, src1.h));
        assert(new1.h != src1.h)
    });

    it("Testing merge_json()", function() {
        let sub = { sub1: 1, sub2: { sub3: 3, sub4: 4 } };
        let src1 = { a:1, b:2, c: { d:1, e:[2,3,4], f:"test" }, g:sub, h:[sub,sub] };
        let dest = { org1:1, org2:{ org3:sub }, a:"old_a", b:"old_b", c:{ d:"old_d", e:"old_e", f:"old_f" }, g:"old_g", h:["old_h1", "old_h2"] };

        let dest1 = z.copy_json(dest);
        z.merge_json(dest1, src1, true, true);
        let s1 = JSON.stringify(dest1);
        assert(!s1.includes("old"));  // all olds should be overwritten
        assert(s1.includes("org1"));  // new values there
        assert(s1.includes("org2"));
        assert(s1.includes("test"));
        assert(s1.includes('"a":1'));
        assert(s1.includes('"org3":{'));

        dest1 = z.copy_json(dest);
        z.merge_json(dest1, src1, true, false);
        s1 = JSON.stringify(dest1);
        assert(!s1.includes("old_a")); // plain values overwritten by new values
        assert(!s1.includes("old_b"));
        assert(!s1.includes("old_d"));
        assert(!s1.includes("old_f"));
        assert(s1.includes("old_e"));  // plain values NOT overwritten by objects
        assert(s1.includes("old_g"));
        assert(s1.includes("old_h1"));
        assert(s1.includes("old_h2"));
        assert(s1.includes("org1"));   // new values there
        assert(s1.includes("org2"));
        assert(s1.includes("test"));
        assert(s1.includes('"a":1'));
        assert(s1.includes('"org3":{'));

        dest1 = z.copy_json(dest);
        z.merge_json(dest1, src1, false, false);
        s1 = JSON.stringify(dest1);
        assert(s1.includes("old_a")); // plain values NOT overwritten by new values
        assert(s1.includes("old_b"));
        assert(s1.includes("old_d"));
        assert(s1.includes("old_f"));
        assert(s1.includes("old_e"));  // plain values NOT overwritten by objects
        assert(s1.includes("old_g"));
        assert(s1.includes("old_h1"));
        assert(s1.includes("old_h2"));
        assert(s1.includes("org1"));   // new values there
        assert(s1.includes("org2"));
        assert(s1.includes('"org3":{'));
    });

    it("Testing generate_rec_folder_name()", function() {
        let now = new Date(2019, 3, 15, 1, 2, 30);

        assert(z.generate_rec_folder_name("abc", 1, 1, 1, '-', '-', '-', now) == "abc-2019-04-15-01-02-30");
        assert(z.generate_rec_folder_name("abc", 1, 1, 0, '-', '-', '-', now) == "abc-2019-04-15-01-02");
        assert(z.generate_rec_folder_name("abc", 1, 0, 0, '-', '-', '-', now) == "abc-2019-04-15");
        assert(z.generate_rec_folder_name("abc", 1, 0, 1, '-', '-', '-', now) == "abc-2019-04-15");
        assert(z.generate_rec_folder_name("abc", 0, 0, 0, '-', '-', '-', now) == "abc");
        assert(z.generate_rec_folder_name("abc", 0, 1, 1, '-', '-', '-', now) == "abc-01-02-30");
        assert(z.generate_rec_folder_name("abc", 0, 1, 0, '-', '-', '-', now) == "abc-01-02");

        assert(z.generate_rec_folder_name("abc", 1, 1, 1, '_', '.', ',', now) == "abc_2019.04.15_01,02,30");
        assert(z.generate_rec_folder_name("abc", 1, 1, 0, '_', '.', ',', now) == "abc_2019.04.15_01,02");
        assert(z.generate_rec_folder_name("abc", 1, 0, 0, '_', '.', ',', now) == "abc_2019.04.15");
        assert(z.generate_rec_folder_name("abc", 1, 0, 1, '_', '.', ',', now) == "abc_2019.04.15");
        assert(z.generate_rec_folder_name("abc", 0, 0, 0, '_', '.', ',', now) == "abc");
        assert(z.generate_rec_folder_name("abc", 0, 1, 1, '_', '.', ',', now) == "abc_01,02,30");
        assert(z.generate_rec_folder_name("abc", 0, 1, 0, '_', '.', ',', now) == "abc_01,02");

        now = new Date(2025, 11, 9, 10, 20, 7);

        assert(z.generate_rec_folder_name("abc/", 1, 1, 1, '-', '-', '-', now) == "abc/2025-12-09-10-20-07");
        assert(z.generate_rec_folder_name("abc/", 1, 1, 0, '-', '-', '-', now) == "abc/2025-12-09-10-20");
        assert(z.generate_rec_folder_name("abc/", 1, 0, 0, '-', '-', '-', now) == "abc/2025-12-09");
        assert(z.generate_rec_folder_name("abc/", 1, 0, 1, '-', '-', '-', now) == "abc/2025-12-09");
        assert(z.generate_rec_folder_name("abc/", 0, 0, 0, '-', '-', '-', now) == "abc/");
        assert(z.generate_rec_folder_name("abc/", 0, 1, 1, '-', '-', '-', now) == "abc/10-20-07");
        assert(z.generate_rec_folder_name("abc/", 0, 1, 0, '-', '-', '-', now) == "abc/10-20");

    });

});

