/**************************************************************************
*
* Copyright 2018-2021 ZD Rpety - DAKEL, Czech Republic
*
***************************************************************************
* This program is a property of Dakel.cz. You may use it after receiving
* written permission from dev@dakel.cz.
***************************************************************************
*
* zedo-rpc module index
*
***************************************************************************/

module.exports.ZedoRPC = require('./zedo-rpc.js');
