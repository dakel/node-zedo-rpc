/**************************************************************************
*
* Copyright 2018-2021 ZD Rpety - DAKEL, Czech Republic
*
***************************************************************************
* This program is a property of Dakel.cz. You may use it after receiving
* written permission from dev@dakel.cz.
***************************************************************************
*
* zedo-rpc module
*
***************************************************************************/

const rpc = require('rpc-websockets');
const assert = require('assert');
const fs = require('fs');

/**
 * ZedoRPC is the main ZEDO RPC client class used to connect to Dakel ZEDO system.
 * Dakel ZEDO is an advanced AE measurement system scalable from a single channel to hundreds
 * of channels enabling AE signal acquisition and analysis, AE hit detection, AE events
 * localization and more.
 *
 * Dakel ZEDO is controlled by a software called ZDaemon (ZEDO Diagnostic AE Monitor) which runs
 * on a Windows-based PC with rich graphical interface. The ZDaemon exports many services over
 * a JSON-RPC API which can be accessed from external applications coded in any programming language.
 *
 * This class provides low-level communication and utility methods to access the JSON-RPC interface
 * and also high-level methods mapped to services provided by ZDaemon software. You can distinguish
 * the two kinds of method easily by naming convention. The low-level methods are named in
 * lower case with underscores (e.g. read_config) while the ZDaemon wrapper methods are named
 * in so-called CamelCase to match the RPC method names (e.g. GetSensors).
 *
 * @param { Object } config - Configuration options
 * @public
 *
 * @example
 * // Configuration object description:
 * {
 *     required_version: 390,             // minimum ZDaemon version required for this application
 *
 *     "rpc": {
 *         "url":"ws://localhost:40999",  // URL to use when connecting to ZEDO RPC server
 *
 *         "options": {                   // RPC client options see https://www.npmjs.com/package/rpc-websockets
 *             "reconnect": false,        // reconnect automatically when connection is lost
 *             "reconnect_interval":1000, // reconnect interval
 *             "max_reconnects": 10       // maximum number of reconnect attempts
 *         }
 *     },
 *
 *     "debug": {
 *         "log_verbosity": 2             // verbosity: 0=off, 1=errors only, 2=information, 3=all
 *     }
 * }

 * @example
 * const zedorpc = require('zedo-rpc');
 *
 * let zedo = new zedorpc.ZedoRPC( { required_version: 378 } );
 * zedo.connect()
 * .then(main)
 * .catch((err) => console.log("Something has failed: " + err + " (msg=" + err.message + ")"))
 * .finally(() => zedo.close());
 *
 * async function main()
 * {
 *     return zedo.GetSystemTime()
 *         .then((res) => {
 *             console.log("Global nanosecond time: " + res.gtime + " Local time: " + res.local_time);
 *         })
 * }
 *
 */
class ZedoRPC {
    /**
     * Create new ZedoRPC object.
     **/
    constructor(config) {
        //super();

        this.rpc_client = null;
        this.logging = this.LOG_NONE;
        this.app_version = "0.0.0.0";

        // default config from file
        this.read_config();

        // runtime config takes precedence
        if (config) {
            this.merge_json(this.config, config);

            if (this.logging >= this.LOG_ALL) {
                console.log("ZEDO-RPC: Configuration after runtime changes:");
                console.dir(this.config, { "depth": null, "colors": true });
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // Utility functions

    /**
     * Read JSON configuration file, synchronous method.
     * @method
     * @param  {String} filename The file name to read, default = *zedo_config.json*
     * @return {Boolean} Success or Failure
     * @public
     *
     * @example
     * const zedorpc = require('zedo-rpc');
     * let zedo = new zedorpc.ZedoRPC();   // this tries to read the default zedo_config.json
     * zedo.read_config("my_config.json"); // read an alternate file
     */
    read_config(filename) {
        let ok = false;

        if (!filename)
            filename = "zedo_config.json";

        try {
            let rawdata = fs.readFileSync(filename);
            this.config = JSON.parse(rawdata);

            if (this.config.debug.log_verbosity && this.config.debug.log_verbosity > this.logging)
                this.logging = this.config.debug.log_verbosity;

            if (this.logging >= this.LOG_ALL) {
                console.log("ZEDO-RPC: Configuration loaded from " + filename);
                console.dir(this.config, { "depth": null, "colors": true });
            }

            ok = true;
        }
        catch (err) {
            console.log("ZEDO-RPC: Error reading the configuration file '" + filename + "'. " + err.toString())
            ok = false;
        }

        return ok;
    }

    ///////////////////////////////////////////////////////////////////////////
    // Low-level communication functions

    /**
     * Establish websocket connection to ZEDO-RPC server
     *
     * @param  { String } url - optional ZEDO-RPC server URL, defaults to config.rpc.url
     * @return { Promise } Asynchronous promise object
     * @public
     */
    async connect(url) {
        if (!this.config.rpc)
            this.config.rpc = {};

        if (!this.config.rpc.options)
            this.config.rpc.options = {};

        if (!url)
            url = this.config.rpc.url;

        if (this.logging >= this.LOG_INFO)
            console.log("ZEDO-RPC: Connecting at " + url);

        this.config.rpc.options.autoconnect = false;
        this.rpc_client = new rpc.Client(url, this.config.rpc.options);

        const self = this;
        return new Promise(function (resolve, reject) {
            self.rpc_client.on("open", () => resolve());
            self.rpc_client.on("error", () => reject("ZEDO-RPC: RPC Could not Connect"));
            self.rpc_client.on("close", () => reject("ZEDO-RPC: RPC Connection Closed"));
            self.rpc_client.connect();
        }).then(() => {
            return this.call("GetAppInfo")
                .catch((err) => {
                    return { "app_version": 0, "msg": "this is an emulated response for very old ZDaemon versions" };
                })
                .then((info) => {
                    self.app_version = info.app_version;
                    let req_ver = self.config.required_version || 0;
                    if (this.logging >= this.LOG_INFO && req_ver > 0)
                        console.log("ZDaemon version " + info.app_version + " (required: " + req_ver + ")");
                    if (self.compare_versions(info.app_version, req_ver) < 0) {
                        const err = "ZEDO-RPC: ZDaemon version is too old; required: " +
                            req_ver + ", actual: " + info.app_version;
                        throw err;
                    }
                    else {
                        return true;
                    }
                })
        });
    }

    /**
     * Disconnect from ZEDO-RPC server
     *
     * @public
     */
    close() {
        if (this.logging >= this.LOG_INFO)
            console.log("ZEDO-RPC: Disconnecting");

        if (this.rpc_client)
            this.rpc_client.close();

        this.rpc_client = null;
    }

    /**
     * Internal RPC error handler
     *
     * @private
     */
    onerror(method, err) {
        if (this.logging >= this.LOG_ERR)
            console.log("ZEDO-RPC: Error when calling method " + method + ": " + err.message || "Unknown");

        // continue routing
        throw err;
    }

    /**
     * Internal handler for successful RPC calls
     *
     * @private
     */
    oncalled(method, ok) {
        if (this.logging >= this.LOG_ALL)
            console.log("ZEDO-RPC: Call of method " + method + " returned: " + JSON.stringify(ok));

        return ok;
    }

    /**
     * Invoke method at ZEDO RPC server
     * @param { String } method The RPC method to be called
     * @param { Object } params Parameters to be passed to the method
     * @return { Promise } Asynchronous promise object
     * @public
     */
    async call(method, params) {
        assert(this.rpc_client, "ZEDO-RPC: Call connect() first");

        if (this.logging >= this.LOG_ALL)
            console.log("ZEDO-RPC: Calling method " + method + "(" + (params ? JSON.stringify(params) : "null") + ")");

        return this.rpc_client.call(method, params)
            .then((ok) => this.oncalled(method, ok))
            .catch((err) => this.onerror(method, err));
    }

    ///////////////////////////////////////////////////////////////////////////
    // Helper calls

    /**
     * Lookup an array element by matching name property
     * @param { Array } items - Array of items which each needs to have a 'name' member
     * @param { String|String[] } name - Name or array of names to lookup
     * @return { Object|Object[] } The item with matching name (or an array of items when name was also an array).
     * @private
     */
    async find_item_by_name(items, name) {
        if (!items || !(items instanceof Array))
            return null;

        let is_array = name instanceof Array;
        let names = is_array ? name : [name];
        let ret = new Array();

        for (var n = 0; n < names.length; n++)
            for (var i = 0; i < items.length; i++)
                if (items[i].name && items[i].name === names[n])
                    ret.push(items[i]);

        return this.get_array_or_item(ret, is_array);
    }

    /**
     * Convert input items (which may be an array or a single item) into result array or single item
     * @param { Array|Object } items One item or array of items
     * @param { Boolean } want_array When true, caller expects an array. When False, caller expects one item.
     * @return { Array|Object } The array or single item (depending on want_array value)
     * @private
     */
    get_array_or_item(items, want_array) {
        if (items instanceof Array) {
            if (want_array) {
                return items;
            }
            else {
                assert(items.length <= 1, "get_array_or_item expects an array here");
                return items.length ? items[0] : null;
            }
        }
        else {
            if (want_array) {
                if (items)
                    return [items];
                else
                    return [];
            }
            else {
                return items;
            }
        }
    }

    /**
     * Extract 'name' members from array of objects
     * @param { Object[] } items - Input array of objects
     * @return { String[] } The array of names extracted from items
     * @private
     */
    get_names(items) {
        let a = new Array();

        if (items instanceof Array) {
            for (var i = 0; i < items.length; i++)
                a.push(items[i].name);
        }

        return a;
    }

    /**
     * Call of setTimeout wrapped into a Promise
     * @param { Number } t - timeout value
     * @return { Promise } '.then' returns after t milliseconds elapse
     * @private
     */
    async delay(t) {
        return new Promise(resolve => setTimeout(resolve, t));
    }

    /**
     * Helper call to print array values to string
     * @param { Object } arr - input array
     * @return { String } array printed
     * @private
     */
    array_to_string(arr) {
        if (!arr instanceof Array)
            return "[Not an Array!]"

        let s = "[";
        var i;

        for (i = 0; i < arr.length; i++) {
            if (i)
                s = s + ", ";
            else
                s = s + " ";

            s = s + arr[i].toString();
        }

        if (i)
            s = s + " ";

        s = s + "]";
        return s;
    }

    /**
     * Helper call to get date-time folder name
     * @param { String } prefix - Folder name prefix
     * @param { Boolean } use_date - Use date in the name, default true
     * @param { Boolean } use_hm - Use time HH:MM in the name, default true
     * @param { Boolean } use_sec - Use time HH:MM:SS in the name, default false
     * @param { String }  name_delimiter - delimiter between prefix, date and time portions
     * @param { String }  date_delimiter - delimiter between year, month and day values
     * @param { String }  time_delimiter - delimiter between hour, minute and second values
     * @param { Date }  now - Time value to use. Uses current time when not specified
     * @return { String } Generated folder name
     * @public
     */
    generate_rec_folder_name(prefix, use_date = true, use_hm = true, use_sec = false, name_delimiter = '-', date_delimiter = '-', time_delimiter = '-', now = null) {
        let name = prefix || "";

        if (!now)
            now = new Date();

        // return true when need to add delimiter to path
        let need_delimiter = function (str, d) {
            let last_c = (str && str.length > 0) ? str.charAt(str.length - 1) : null;
            if (!d)
                return false;
            return last_c && last_c != '/' && last_c != '\\' && last_c != d;
        }

        let fmt = function (n) {
            let s = n.toString();
            return s.length == 1 ? "0" + s : s;
        }

        if (use_date) {
            if (need_delimiter(name, name_delimiter))
                name += name_delimiter;
            let month = now.getMonth() + 1;
            name += now.getFullYear() + date_delimiter + fmt(month) + date_delimiter + fmt(now.getDate());
        }

        if (use_hm) {
            if (need_delimiter(name, name_delimiter))
                name += name_delimiter;
            name += fmt(now.getHours()) + time_delimiter + fmt(now.getMinutes());

            if (use_sec) {
                name += time_delimiter + fmt(now.getSeconds());
            }
        }

        return name;
    }

    /**
     * Compare version or build strings
     * @param { String } ver_a - Version A, format "X.Y", "X.Y.Z" or "X.Y.Z.BUILD" or just BUILD number
     * @param { String } ver_b - Version B, same format options as above
     * @return { Number } -1, 0, +1 as a traditional compare result
     * @public
     */
    compare_versions(ver_a, ver_b) {
        let a = ver_a.toString().split("\.");
        let b = ver_b.toString().split("\.");

        let build_a = a.length == 1 ? a[0] : a.length >= 4 ? a[3] : null;
        let build_b = b.length == 1 ? b[0] : b.length >= 4 ? b[3] : null;

        // only compare BUILD numbers when at least one of them is pure BUILD value
        if (build_a !== null && build_b !== null && (a.length == 1 || b.length == 1))
            return build_a < build_b ? -1 : build_a > build_b ? 1 : 0;

        // compare by parts (undefined parts are zero)
        a.push(0, 0, 0);
        b.push(0, 0, 0);
        for (var i = 0; i < 4; i++) {
            if (a[i] != b[i])
                return a[i] < b[i] ? -1 : 1;
        }

        // same versions
        return 0;
    }

    /**
     * Make sure the ZDaemon app is at specific version or above
     * @param { String|Number } min_version - Version A, format "X.Y", "X.Y.Z" or "X.Y.Z.BUILD" or just BUILD number
     * @return This function throws an exception if lower version is detected
     * @public
     */
    assert_min_version(min_version, assert_name) {
        if (this.compare_versions(this.app_version, min_version) < 0) {
            if (!assert_name)
                assert_name = "This function";
            throw (assert_name + " requires ZDaemon version " + min_version + " or above");
        }
    }


    /**
     * Return a deep copy of given object using JSON stringify/parse methods
     * @param { Object } obj - Input object
     * @return { Object } cloned object
     * @public
     */
    copy_json(obj) {
        if (this.is_json_object(obj))
            return JSON.parse(JSON.stringify(obj));
        else
            return obj; // basic type does not need to be deeply copied
    }

    /**
     * Print formatted JSON object to console
     * @param { Object } obj - Input object
     * @public
     */
    print_json(obj) {
        console.dir(obj, { "depth": null, "colors": true });
        return obj;
    }

    /**
     * Determine if given object contains any "own" property, i.e. is an non-empty JSON object
     * @param { Object } obj - The input object
     * @return { Object } true if obj is a JSON object
     * @public
     */
    is_json_object(obj) {
        if (typeof obj == "object") {
            for (var key in obj) {
                if (obj.hasOwnProperty(key))
                    return true;
            }
        }
        return false;
    }

    /**
     * Merge one JSON to other
     * @param { Object } dest - Destination object
     * @param { Object } src - Source object
     * @param { Boolean } overwrite_values - Values of basic-types will overwrite existing keys when they are also basic-types
     * @param { Boolean } overwrite_objects - Source object may overwrite destination members of non-matching types, i.e. changing object structure
     * @return { Object } the destination object with new (deep-copied) values merged from source object
     * @public
     */
    merge_json(dest, src, overwrite_values = true, overwrite_objects = false) {
        for (const key in src) {
            if (src.hasOwnProperty(key)) {
                if (dest.hasOwnProperty(key)) {
                    if (this.is_json_object(dest[key])) {
                        // when both are objects, merge recursively
                        if (this.is_json_object(src[key]))
                            this.merge_json(dest[key], src[key], overwrite_values, overwrite_objects);
                        else if (overwrite_objects)
                            dest[key] = src[key];
                    }
                    else {
                        if (this.is_json_object(src[key])) {
                            if (overwrite_objects)
                                dest[key] = this.copy_json(src[key]);
                        }
                        else {
                            if (overwrite_values)
                                dest[key] = src[key];
                        }
                    }
                }
                else {
                    // adding new key
                    if (this.is_json_object(dest[key]))
                        dest[key] = this.copy_json(src[key]);
                    else
                        dest[key] = src[key];
                }
            }
        }

        return dest;
    }

    /**
     * Determine if local path is a ZEDO data directory (zedodata.zdat file present)
     * @param { String } path - Source data directory or directories to open.
     * @return { Boolean } test result
     * @public
     */
    is_zdat_directory(path) {
        var ret = false;

        try {
            let stat = fs.lstatSync(path);

            if (stat.isDirectory()) {
                let zdat = fs.lstatSync(path + "\\zedodata.zdat")
                ret = zdat.isFile();
            }
        }
        catch (err) {
            ret = false;
        }

        return ret;
    }

    ///////////////////////////////////////////////////////////////////////////
    // High-level functions (RPC calls)

    /**
     * ZEDO RPC call: Obtain array of sensor configurations
     * @param { String } verbosity - Required verbosity of output; examples: "min", "max", "gain|pulser", .. default is "all"
     * @return { Promise<Object> } '.then' returns current configuration objects in required verbosity
     * @public
     * @example
     * // Possible verbosity values, multiple values may be combined, separated by | character
     * // See "Configure" method for more details about the configuration object
     * //   - 'min'  ... name, enabled and status information only
     * //   - 'max'  ... full sensor configuration
     * //   - 'all'  ... full configuration including also all "obsolete" values for backward compatibility
     * //   - 'gain'  .. information about gain
     * //   - 'pulser' . pulser information
     * //   - 'triggers' all trigger information
     * //   - 'sigsmp' - continuous sampling and other sampler configuration
     * //   - 'hitdets'  information about all hitdetectors
     * //   - 'hitdet0'..'hitdet2' information about selected hit detector
     * //   - 'hitdet_calib' calibration hit detector
     * //   - 'filters' . digital filter configuration
     * //
     * zedo.GetSensors("all")  // get all entries including the obsolete ones
     *   .then((res) => console.log(res))
     * @example
     * // retrieve gain and pulser information
     * zedo.GetSensors("gain|pulser")
     *   .then((res) => console.log(res))
     */
    async GetSensors(verbosity) {
        return this.call("GetSensors", verbosity ? { "verbosity": verbosity } : null);
    }

    /**
     * ZEDO RPC call: Get system status overview, number of dead/alive units etc.
     * @return { Promise<Object> } '.then' returns the response object
     * @public
     * @example
     * let stat = zedo.GetSystemStatus();
     *
     * // Example of returned object
     * {
     *     boards: {                   // information about all ZEDO boards
     *         total: 3,               // total number of boards
     *         disabled: 0,            // number of disabled boards
     *         dead: 1,                // number of boards which do not respond
     *         alive: 2,               // number of active boards
     *         time_master: 1,         // number of boards acting as time master
     *         time_slave_valid: 1,    // number of boards acting as time slave
     *         time_slave_invalid: 0   // number of boards with invalid time
     *     },                          //
     *     bunits: {                   // information about board units
     *         total: 6,               // total number of units
     *         disabled: 0,            // number of disabled units
     *         dead: 2,                // number of units on non-responsive boards
     *         alive: 4                // number of enabled and active units
     *     },                          //
     *     bunits_ae: {                // information about AE board units (AE channels)
     *         total: 6,               // total number of AE units
     *         disabled: 0,            // number of disabled AE units
     *         dead: 2,                // number of AE units on non-responsive boards
     *         alive: 4                // number of enabled and active AE units
     *     },                          //
     *     status: 0                   // operation status, >=0 success, <0 error
     * }
     */
    async GetSystemStatus() {
        return this.call("GetSystemStatus");
    }

    /**
     * ZEDO RPC call: Get system time in both hardware nanosecond time and in formatted local time
     * @return { Promise<Object> } '.then' returns the response object
     * @public
     * @example
     * let time = zedo.GetSystemTime();
     *
     * // Example of returned object
     * {
     *     gtime: "598219986194000000", // current global time as number of nanoseconds since 1.1.2010
     *     local_date: "15.12.2018",    // current global time converted to date string
     *     local_time: "20:13:06.194",  // current global time converted to time string
     *     status: 0                    // operation status, >=0 success, <0 error
     * }
     */
    async GetSystemTime() {
        return this.call("GetSystemTime");
    }

    /**
     * ZEDO RPC call: Obtain board unit (AE sensor) configurations
     * @param { String|String[] } name - Name of sensor (=board unit) or array of names for sensors for which
     *                     a configuration is to be returned
     * @param { String } verbosity - Required verbosity of output; examples: "min", "gain|pulser", .. default is "all"
     * @return { Promise<Object|Object[]> } '.then' returns configuration object or array of objects (depending on input type)
     * @public
     * @example
     * // get configuration of selected board units
     * let cfg = zedo.GetConfiguration([ "1.0A", "1.0B, "1.1A" ], "max")
     *
     * // Example of returned object (or array of objects) obtained with "max" verbosity,
     * // NOTE: the same configuration object is returned by GetSensors method,
     * //       use the same object properties when calling the "Configure" method
     *
     * [
     *   {
     *      name: '1.0A',               // key identifier of the board unit (matches "name" input parameter)
     *      gain: {                     // information about AE sensor gain
     *          total: 34,              // total amplification in dB
     *          lsb_v: 3.0445286788465572e-5,  // value voltage representing one step of ADC converter
     *          amp: 0,                 // main (on-board) amplifier gain in dB
     *          preamp: 34,             // information about pre-amplifier gain
     *          corr: 0,                // correction gain in dB specified by user
     *          power_mode: '12V/820R', // pre-amplifier power: "off", "12V/820R", "24V/820R", "12V/50R", "24V/50R"
     *          info_current_a: 0       // current to pre-amplifier in Amperes
     *      },
     *      hitdets: {                  // hit detectors
     *          hitdet0: {              // hit detector 0
     *             threshold: 'auto200%', // hit threshold, possible values:
     *                                  //   "off" ... hit detector de-activated
     *                                  //   "auto112%", "auto125%", "auto150%", "auto200%" ... floating threshold
     *                                  //   "20LSB" ... threshold in ADC converter values
     *                                  //   "3uV"   ... threshold in Voltage units (use with uV, mV, V untis)
     *                                  //   "40dBAE" .. threshold in dBAE untis
     *                                  //   "60%"  .... threshold in percentage of ADC full range
     *             separation: 1e-8,    // hit separation time in seconds
     *             deadtime: 0.01,      // hit detection dead-time in seconds
     *             maxlen: 0.5,         // maximum possible hit length in seconds
     *             minlen: 0.00005,     // minimum possible hit lenght in seconds
     *             ftperiod: 0.05       // period for updating floating threshold
     *          },
     *          hitdet1: {
     *             // ... same content as hitdet0
     *          },
     *          hitdet2: {
     *             // ... same content as hitdet0
     *          },
     *          hitdet_calib: {
     *             // ... same content as hitdet0
     *          },
     *      },
     *      filters: {                  // digital FIR filter settings
     *          hp: { index: 0 },       // High-pass filter index
     *          lp: { index: 12 }       // Low-pass filter index
     *      },
     *      triggers: {                 // configuration of sampler triggers
     *          threshold: {            // "Threshold" trigger configuration
     *            bind: 'self',
     *            pre_trig: 0.002,
     *            post_trig: 0.01,
     *            thr: '50%',
     *            holdoff: 0.001
     *          },
     *          timeout: {              // "Automatic" timeout trigger configuration
     *            bind: 'board', pre_trig: 0, post_trig: 0.005, // same options as described above
     *            period: 3,            // timeout trigger time in seconds
     *            auto_reset: 1,        // reset timeout trigger when other trigger source fires
     *            gtime_sync: 0         // synchronize the timeout measurement with global sync pulse
     *          },
     *          // the following trigger sources have the options already described above
     *          external0: { bind: 'box', pre_trig: 0.002, post_trig: 0.01 }, // External trigger 0
     *          external1: { bind: '', pre_trig: 0.002, post_trig: 0.01 },    // External trigger 1
     *          hitdet0: { bind: '', pre_trig: 0.001, post_trig: 0.001 },     // Trigger by Hit Detector 0
     *          hitdet1: { bind: '', pre_trig: 0.001, post_trig: 0.001 },     // Trigger by Hit Detector 1
     *          hitdet2: { bind: '', pre_trig: 0.001, post_trig: 0.001 },     // Trigger by Hit Detector 2
     *          network: { bind: '', pre_trig: 0, post_trig: 0 }              // Incoming network trigger
     *      },
     *      sigsmp: {                   // signal sampler configuration
     *         continuous: 0            // enable continuous sampling: 0=off, 1=only when recording, 2=always enabled
     *      },
     *      pulser: {                   // pulser information and configuration
     *         info_installed: 1        // boolean value, non-zero when pulser is available
     *         info_slot: 8,            // box slot with pulser
     *         info_hwid: 1,            // hardware pulser version
     *         pulser_mode: "off",      // pulser operation mode, possible values:
     *                                  //    "off" .... pulser OFF
     *                                  //    "passive"  passive relay state (use when pulsing on multiple sensors)
     *                                  //    "sin" .... sine wave generator of fixed frequency 'freq_hz'
     *                                  //    "sweep" .. sine wave, frequency sweeping between 'freq_hz' and 'fmax_hz'
     *                                  //    "burst" .. isolated sine wave bursts of fixed frequency 'freq_hz'
     *                                  //    "bus" .... bus playback performed by AE sensor
     *                                  //    "bnc" .... external signal
     *         offset_mode: "+15V",     // output voltage offset applied, possible values:
     *                                  //    "+15V", "+18V", "0V", "-5V"
     *         amp_v: 30,               // pulser signal amplitude in Volts (3..30)
     *         freq_hz: 100000,         // frequency of sine wave generator (use with "sin", "sweep", "burst" modes)
     *         fmax_hz: 200000,         // limit sweeping frequency
     *         sweep_time: 5,           // frequency sweep duration in seconds
     *         sweep_steps: 10,         // number of steps for frequency sweeping
     *         burst_shape: "rect",     // gating shape for burst mode, only "rect" currently supported
     *         burst_len: 10e-3,        // duration of burst signal in seconds
     *         burst_rep: 1             // period of repeating the burst signal
     *      },
     *      enabled: 1                  // non-zero when board unit is enabled
     *   },
     *   // following objects in array (one configuration object per each requested board unit name)
     *   { name: "1.0B", ...},
     *   { name: "1.1A", ...},
     *   { name: "1.1B", ...},
     *   // ...
     * ] // end of array
     */
    async GetConfiguration(name, verbosity) {
        return this.call("GetSensors", verbosity ? { "verbosity": verbosity } : null)
            .then((ok) => this.find_item_by_name(ok.items, name));
    }

    /**
     * ZEDO RPC call: Configure one or more sensors, return the actual configuration objects
     * @param { Object|Object[] } config - One object or array of objects to configure ('name' must be a member)
     * @param { String } verbosity - Required verbosity of output; examples: "min", "gain|pulser", .. default is "all"
     * @return { Promise } '.then' returns confirmed configuration object or array of configuration objects
     *                     (depending on input type)
     * @public
     * @example
     * // configure the gain property of one selected unit and request the actual gain parameters back
     * zedo.Configure( { "name":"1.0A", "gain": { "amp":20, "pre_amp":34, "power_mode":"12V/820R" } }, "gain" );
     * @example
     * // turn on a sine-wave burst generator pulser
     * zedo.Configure( { "name":"1.0A", "pulser": { "pulser_mode":"burst",
     *                   "offset_mode":"+15V", "amp_v":10, "freq_hz":100000,
     *                   "burst_len":10e-3, "burst_rep":0.4 } }, "pulser" );
     *
     * // NOTE: See the GetConfiguration method for the full description of the configuration object
     * // The response object also contains the "status" member with configuration result
     * // status >=0 for success, <0 for an error
     */
    async Configure(config, verbosity) {
        // make it an array
        let is_array = config instanceof Array;
        let items = is_array ? config : [config];

        for (var i = 0; i < items.length; i++)
            if (!items[i].type)
                items[i].type = "bunit";

        return this.call("Configure", { "items": items, "verbosity": verbosity })
            .then((ok) => this.get_array_or_item(ok.items, is_array));
    }

    /**
     * ZEDO RPC call: Clear live data and reset relative measurement time to 0
     * @return { Promise } '.then' returns the response object (check .status to be zero)
     * @public
     */
    async ClearLiveData() {
        return this.call("ClearLiveData");
    }

    /**
     * Internal handling Recording response structure
     *
     * @private
     */
    process_recording_status(name, resp) {
        if (resp.status < 0 && this.logging >= this.LOG_ERR) {
            console.log("ZEDO-RPC: " + name + " failed with status:" + resp.status + ", current recording state: " + resp.recording_mode);
        }

        return resp;
    }

    /**
     * ZEDO RPC call: Start data recording
     * @param { String } measurement_name - optional, name of the recording. ZDaemon will decide
     *                   its own name if not specified.
     * @param { Number } record_history_secs - optional, number of seconds to record from data buffers.
     * @return { Promise } '.then' returns the response object (check .status to be zero)
     * @public
     * @example
     * zedo.StartRecording((resp) => { console.log(resp); });
     *
     * // Example of returned object
     * {
     *     recording_mode: 2,        // current recording mode: 0=stopped, 1=paused, 2=recording
     *     status: 0                 // operation status, >=0 success, <0 error
     * }
     */
    async StartRecording(measurement_name, record_history_secs = 0) {
        return this.call("StartRecording", {
            "recording_name": measurement_name,
            "history_sec": record_history_secs
        })
            .then((resp) => this.process_recording_status("StartRecording", resp));
    }

    /**
     * ZEDO RPC call: Pause data recording
     * @return { Promise } '.then' returns the response object (check .status to be zero)
     * @public
     * @example
     * zedo.PauseRecording((resp) => { console.log(resp); });
     *
     * // Example of returned object
     * {
     *     recording_mode: 1,        // current recording mode: 0=stopped, 1=paused, 2=recording
     *     status: 0                 // operation status, >=0 success, <0 error
     * }
     */
    async PauseRecording() {
        return this.call("PauseRecording")
            .then((resp) => this.process_recording_status("PauseRecording", resp));
    }

    /**
     * ZEDO RPC call: Stop data recording
     * @return { Promise } '.then' returns the response object (check .status to be zero)
     * @public
     * @example
     * zedo.StopRecording((resp) => { console.log(resp); });
     *
     * // Example of returned object
     * {
     *     recording_mode: 0,        // current recording mode: 0=stopped, 1=paused, 2=recording
     *     status: 0                 // operation status, >=0 success, <0 error
     * }
     */
    async StopRecording() {
        return this.call("StopRecording")
            .then((resp) => this.process_recording_status("StopRecording", resp));
    }

    /**
     * ZEDO RPC call: Determine recording status
     * @return { Promise } '.then' returns the response object (check .status to be zero)
     * @public
     * @example
     * let rec = zedo.GetRecordingState();
     *
     * // Example of returned object
     * {
     *     recording_mode: 0,        // current recording mode: 0=stopped, 1=paused, 2=recording
     *     recording_mode_text: "Stopped",  // current recording mode in text format
     *     recording_root: "C:\\ZEDO_Data\\experiment", // current recording data folder
     *     status: 0                 // operation status, >=0 success, <0 error
     * }
     */
    async GetRecordingState() {
        return this.call("GetRecordingState");
    }

    /**
     * ZEDO RPC call: Get ZDaemon application information
     * @return { Promise<Object> } '.then' returns the information object
     * @public
     * @example
     * let info = zedo.GetAppinfo();
     *
     * // Example of returned object
     * {
     *     app_version: "1.1.0.381", // ZDaemon application version in form of X.Y.Z.BUILD
     *     lic_seed: "0x9CF859B3",   // license seed number
     *     lic_flags: "0x0012030",   // currently enabled licenses
     *     lic_channs: "0x4",        // number of licensed AE channels
     *     status: 0                 // operation status, >=0 success, <0 error
     * }
     */
    async GetAppInfo() {
        return this.call("GetAppInfo");
    }

    ///////////////////////////////////////////////////////////////////////////
    // Advanced functions with additional/extended processing

    /**
     * Get array of board units with activated pulser
     * @param { Boolean } passives_too - Also include items configured for passive mode
     * @return { Promise } '.then' returns the array of objects with an active pulser
     * @public
     * @example
     * zedo.GetActivePulsers(true).then((resp) => { console.log(resp); });
     *
     * // Example of returned array of objects
     * [
     *     {
     *         name: "1.0A",
     *         pulser: {
     *             pulser_mode: "burst",
     *             offset_mode: "+15V",
     *             amp_v: 10,
     *             freq_hz: 100000,
     *             burst_len: 10e-3,
     *             burst_rep: 0.4
     *         }
     *     },
     *     {
     *         name: "1.1B",
     *         pulser: {
     *             pulser_mode: "passive",
     *         }
     *     }
     * ]
     */
    async GetActivePulsers(passives_too) {
        return this.GetSensors("pulser")
            .then((res) => {
                let pulsers = new Array();
                for (var i = 0; i < res.items.length; i++) {
                    let item = res.items[i];
                    if (item.pulser.pulser_mode && item.pulser.pulser_mode != "off") {
                        if (item.pulser.pulser_mode != "passive" || passives_too)
                            pulsers.push(item);

                    }
                }

                return pulsers;
            });
    }

    /**
     * Make sure all pulsers are off
     * @param { Boolean } passives_too - Also include items configured for passive mode
     * @return { Promise } '.then' returns the array of objects with an active pulser
     * @public
     */
    async AllPulsersOff(passives_too) {
        return this.GetActivePulsers(passives_too)
            .then((items) => {
                let deactivate = new Array();
                for (var i = 0; i < items.length; i++) {
                    let item = items[i];
                    deactivate.push({ "name": item.name, "type": "bunit", "pulser": { "pulser_mode": "off" } });
                }

                if (deactivate.length)
                    return this.Configure(deactivate, "pulser")
                else
                    return deactivate;
            });
    }

    /**
     * Compare if two pulser configurations are the same, ignore irrelevant parameters
     * @param { Object } p1 - Pulser configuration object 1
     * @param { Object } p2 - Pulser configuration object 2
     * @return { Boolean } Returns true when configurations are the same
     * @public
     */
    pulser_configs_same(p1, p2) {
        let diff = function (v1, v2) {
            if (typeof v1 === "undefined" || typeof v2 === "undefined")
                return false;
            if (typeof v1 === "number" && typeof v2 === "number")
                return Math.abs(v1 - v2) >= 1;  // avoid direct comparing of floating point numbers
            return v1 != v2;
        }

        if (!this.is_json_object(p1) || !this.is_json_object(p2))
            return false;

        if (p1.pulser_mode != p2.pulser_mode)
            return false;

        if (p1.pulser_mode != "off" && p1.pulser_mode != "passive") {
            if (diff(p1.offset_mode, p2.offset_mode))
                return false;

            if (p1.pulser_mode == "sin" || p1.pulser_mode == "sweep" || p1.pulser_mode == "burst") {
                if (diff(p1.amp_v, p2.amp_v))
                    return false;
                if (diff(p1.freq_hz, p2.freq_hz))
                    return false;

                if (p1.pulser_mode == "sweep") {
                    if (diff(p1.fmax_hz, p2.fmax_hz))
                        return false;
                    if (diff(p1.sweep_time, p2.sweep_time))
                        return false;
                    if (diff(p1.sweep_steps, p2.sweep_steps))
                        return false;
                }

                if (p1.pulser_mode == "burst") {
                    if (diff(p1.burst_shape, p2.burst_shape))
                        return false;
                    if (diff(p1.burst_len, p2.burst_len))
                        return false;
                    if (diff(p1.burst_rep, p2.burst_rep))
                        return false;
                }
            }
        }

        // all relevant values were the same
        return true;
    }

    /**
     * Apply pulser configuration and verify it is set properly
     * @return { Promise } '.then' returns the array of objects with configured pulser
     * @private
     */
    async configure_pulser_with_retries(pulser_config, retries, retry_count, retry_delay_ms) {
        retry_count++;

        if (this.logging >= this.LOG_ALL) {
            console.log("ZEDO-RPC: Setting pulser '" + pulser_config.name + "' to " + pulser_config.pulser.pulser_mode +
                " (attempt " + retry_count + " of " + (retries + 1) + ")");
        }

        return this.Configure(pulser_config, "min")
            .then((applied_config) => {
                if (this.logging >= this.LOG_ALL)
                    console.log("ZEDO-RPC: Setting pulser: pulser set, now delay " + retry_delay_ms + "ms");

                return this.delay(retry_delay_ms)
                    .then(() => {
                        if (applied_config.status < 0)
                            throw "Pulser configuration cannot be set. Bad status:" + applied_config.status + ".";

                        if (this.logging >= this.LOG_ALL)
                            console.log("ZEDO-RPC: Setting pulser: retrieve new configuration");

                        return this.GetConfiguration(pulser_config.name, "pulser")
                            .then((new_config) => {
                                if (!new_config)
                                    throw "Pulser configuration cannot be set. Bad answer.";

                                if (this.pulser_configs_same(pulser_config.pulser, new_config.pulser)) {
                                    if (this.logging >= this.LOG_ALL)
                                        console.log("ZEDO-RPC: Setting pulser: verified successfully");
                                    return new_config;
                                }
                                else if (retry_count <= retries) {
                                    if (this.logging >= this.LOG_ALL)
                                        console.log("ZEDO-RPC: Setting pulser: pulser NOT confirmed to be in required state, retrying...");
                                    return this.configure_pulser_with_retries(pulser_config, retries, retry_count, retry_delay_ms);
                                }
                                else {
                                    throw "Pulser cannot be set as requested (tried " + retries + "x)!";
                                }
                            });
                    });
            });
    }

    /**
     * Set given board unit to specified pulser mode. This function may attempt to retry if pulser activation fails.
     * @param { String } name - Board unit name which is to be configured.
     * @param { Object } pulser - Pulser configuration object
     * @param { Number } retries - Number of retries
     * @param { Number } retry_delay_ms - Hold time after retrying
     * @return { Promise } '.then' returns the array of objects with configured pulser
     * @public
     */
    async SetPulser(name, pulser, retries = 3, retry_delay_ms = 333) {
        let names = name instanceof Array ? name : [name];

        if (names.length != 1)
            throw "Cannot call SetPulser with more than one item";

        let config = { "name": names[0], "type": "bunit", "pulser": pulser };
        //return this.Configure(config, "pulser")
        return this.configure_pulser_with_retries(config, retries, 0, retry_delay_ms);
    }

    /**
     * Enable or disable continuous recording on selected boards (by names)
     * @param { String|String[] } name - Board unit name or array of names which are to be configured.
     * @param { Number } enable - 0 to disable continuouse recording, 1 to enable it, 2 to enable it when recording
     * @return { Promise } '.then' returns the array of objects with configured continuos recording
     * @public
     */
    async EnableContinuousRecording(name, enable) {
        let names = name instanceof Array ? name : [name];
        let config = new Array();

        for (var i = 0; i < names.length; i++)
            config.push({ "name": names[i], "type": "bunit", "sigsmp": { "continuous": enable } });

        return this.Configure(config, "sigsmp");
    }

    /**
     * ZEDO RPC call: Open new or locate existing FileReader object with specific set of source paths
     * @param { String|String[] } path - Source data directory or directories to open.
     * @return { Promise<Object> } '.then' returns the response object (check .status to be zero)
     * @public
     * @example
     * zedo.OpenFileReaderByPath([ "C:\\Folder1", "C:\\Folder2" ]).then((resp) => { console.log(resp); });
     *
     * // Example of returned object
     * {
     *     _id: Number,      // reader ID
     *     status: 0         // operation status, >=0 success, <0 error
     * }
     */
    async OpenFileReaderByPath(path) {
        this.assert_min_version(486)

        let paths = path instanceof Array ? path : [path];
        return this.call("OpenFileReaderByPath", { "paths": paths });
    }

    /**
     * ZEDO RPC call: Lookup an existing FileReader object of a given name
     * @param { String } name - FileReader object name. If name is empty, the default (first) existing FileReader will be located.
     * @return { Promise<Object> } '.then' returns the response object (check .status to be zero)
     * @public
     * @example
     * zedo.OpenFileReaderByName("MyReader1").then((resp) => { console.log(resp); });
     *
     * // Example of returned object
     * {
     *     _id: Number,      // reader ID
     *     status: 0         // operation status, >=0 success, <0 error
     * }
     */
    async OpenFileReaderByName(name = "") {
        this.assert_min_version(486);
        return this.call("OpenFileReaderByName", { "name": name });
    }

    /**
     * ZEDO RPC call: Re-configure FileReader and assign it a new source path
     * @param { Number } reader_id - File reader ID received from OpenFileReaderXXX methods
     * @param { String|String[] } path - Source data directory or directories to open.
     * @param { Boolean } update_name - Let the file reader to change its name based on the source data path.
     * @return { Promise<Object> } '.then' returns the response object (check .status to be zero)
     * @public
     * @example
     * zedo.SetFileReaderPath(id, "C:\\path", false).then((resp) => { console.log(resp); });
     *
     * // Example of returned object
     * {
     *     status: 0               // operation status, >=0 success, <0 error
     * }
     */
    async SetFileReaderPath(reader_id, path, update_name = false) {
        this.assert_min_version(486);

        let paths = path instanceof Array ? path : [path];
        return this.call("SetFileReaderPath", { "reader_id": reader_id, "paths": paths, "update_name": update_name });
    }

    /**
     * ZEDO RPC call: Get File Reader information
     * @param { Number } reader_id - File reader ID received from OpenFileReaderXXX methods
     * @return { Promise<Object> } '.then' returns the response object (check .status to be zero)
     * @public
     * @example
     * zedo.GetFileReaderInfo(reader_id).then((resp) => { console.log(resp); });
     *
     * // Example of returned object
     * {
     *     reader_id: Number,      // reader ID
     *     name: String,           // reader name
     *     paths: String[],        // list of data folders
     *     gt_base: String,        // base time as nanosecond Global Time (if valid base)
     *     time_base: Number,      // base time as JavaScript time (if valid base)
     *     data: Array,            // same as GetFileReaderData()
     *     status: 0               // operation status, >=0 success, <0 error
     * }
     */
    async GetFileReaderInfo(reader_id) {
        this.assert_min_version(486);
        return this.call("GetFileReaderInfo", { "reader_id": reader_id });
    }

    /**
     * ZEDO RPC call: Get File Reader information, list of boards / units and items
     * @param { Number } reader_id - File reader ID received from OpenFileReaderXXX methods
     * @return { Promise<Array> } '.then' returns the response object
     * @public
     * @example
     * zedo.GetFileReaderData(reader_id).then((resp) => { console.log(resp); });
     *
     * // Example of returned array of objects
     * [
     *     {
     *         name: "1.0",
     *         type: "r_board",
     *         data: [ ...array of data items ]
     *     },
     *     {
     *         name: "1.0A",
     *         type: "r_bunit",
     *         data: [ ...array of data items ]
     *     },
     *     {
     *         name: "1.0B",
     *         type: "r_bunit",
     *         data: [ ...array of data items ]
     *     }
     * ]
     */
    async GetFileReaderData(reader_id) {
        this.assert_min_version(486);
        return this.call("GetFileReaderData", { "reader_id": reader_id });
    }

    /**
     * ZEDO RPC call: Export file reader data
     * @param { Number } reader_id - File reader ID received from OpenFileReaderXXX methods
     * @param { Object } export_cfg - Export configuration. Use ZDaemon GUI to generate a template.
     * @return { Promise<Object> } '.then' returns the response object (check .status to be zero)
     * @public
     * @example
     * zedo.ExportFileReaderData(reader_id, export_cfg).then((resp) => { console.log(resp); });
     *
     * // Example of export_cfg object
     * {
     *     // general export settings
     *     export:
     *     {
     *         allow: String[],  // Array of items to export. Default: All File Reader items are allowed if this is array is null or empty.
     *                                  //   Each item is one of:
     *                                  //   -  object identifier "_id:xxxxxxx" - allow export of all data in this one object and all sub-objects
     *                                  //   - "type" - any of r_xxx types - allow export this type for all items in file reader
     *                                  //      the type may be followed by aninstance number. Examples: r_siginfo, r_hitdet0, r_hitdet1, r_digin0
     *         deny: String[],   // Array of items to skip from export. This list is applied after the 'allow' list. Default: empty
     *         mode: 0..2,       // Export mode: 0=do nothing, validate config only, 1=show export dialog, 2=start background export job. Default:0.
     *     },
     *
     *     // time base and export format
     *     time:
     *     {
     *         base: 0|1|time,       // Time base for relative time calculation. One of the following
     *                               //   - 0 ... time is relative to FileReader base time (Default)
     *                               //   - 1 ... relative to the first point ever seen
     *                               //   - Number ... specific JavaScript time is used as a base time
     *         time_format: 0..1     // Primary time format. 0=default relative time in seconds,  1=nanosecond integer time. Default:0
     *         date_format: 0..2     // Absolute time/date format. 0=not exported, 1=Date/Time, 2=Time only. Default:0
     *     },
     *
     *     // Export of AE Parameters (RMS, Counts, ..)
     *     siginfo:
     *     {
     *         voltage_format: 0..5  // AE voltage in Volts: 0=not exported, 1=(int)nV, 2=(int)uV, 3=(float)uV, 4=(float)mV, 5=(float)V. Default=5.
     *         voltage_dbae: 0..1    // AE voltage in dBAE: 0=not exported, 1=exported. Default=0.
     *         counts_log: 0..1      // AE counts in logarithmic units: 0=not exported, 1=exported. Default=1.
     *         counts_lin: 0..1      // AE counts in linear units: 0=not exported, 1=exported. Default=0.
     *         energy_format: 0..1   // AE energy units: 0=V^2/Hz, 1=uV^2/Hz. Default=1.
     *     },
     *
     *     // Export of AE Hits
     *     hitdet:
     *     {
     *         extra_cols: 0..1     // Hit extra data columns. 0=not exported, 1=all exported. Default:0.
     *     },
     *
     *     // Export of "stream" data like FastRMS, Analog Input etc.
     *     stream:                   // Format of general stream data (FastRMS, Analog input, ...). Note that sigsmp has custom settings.
     *     {
     *         file_format: 0..1     // 0=text only, 1=text+binary. Default=0.
     *         max_size: Number      // Maximum number of samples put to single file. Default:0 (no-limit)
     *     },
     *
     *     // Export of high-speed AE Signal data
     *     sigsmp:
     *     {
     *         mode: 0..2             // AE Signal export mode: 0:hits only, 1:countinuous stream, 2:spectrogram. Default:1.
     *
     *         hit:                   // Hit signal export options. Only used if mode==0.
     *         {
     *             pre_time: Number   // Signal before hit. Time in seconds. Default:1e-3.
     *             post_time: Number  // Signal after hit. Time in seconds. Default:1e-3.
     *         },
     *
     *         stream:                 // Signal stream export options. Only used in mode==1
     *         {
     *             max_size: Number    // Maximum number of samples put to single file. Default:0 (no-limit)
     *         },
     *
     *         output:                 // file parameters for mode==0 or mode==1
     *         {
     *             file_format: 0..2   // 0=text+binary, 1=text+text, 2=single text. Default=0.
     *             gnuplot: 0..1       // 1=Export gnuplot scripts (forces file_format=0). Default=1.
     *             psd_format: 0..1    // Generate spectrum file: 0=not exported, 1=exported. Default=0.
     *
     *             psd:                // Spectrum calculation paraters (only used if psd_format==1)
     *             {
     *                 wnd_size: Number    // PSD window size in points. Default:4096.
     *                 wnd_overlap: Number // PSD window overlap in percents. Default:10.
     *                 wnd_type: 0..x      // PSD window type: 0=Welch, others not yet supported. Default:0.
     *                 wnd_envelope: 0..1  // Use maximum of windows instead of averaging. Default:0.
     *                 freq_min: Number    // Minimum frequency in Hz. Default:0 (unlimited)
     *                 freq_max: Number    // Maximum frequency in Hz. Default:unlimited
     *                 zero_dc: 0..1       // 1=force zero 0 Hz value (clear DC offset)
     *             }
     *         },
     *
     *         spectrogram:            // Spectrogram settings. Only used in mode==2
     *         {
     *             time_step: Number   // Spectrogram time step in seconds. Default:1.
     *             wnd_size: Number    // PSD window size in points. Default:4096.
     *             wnd_overlap: Number // PSD window overlap in percents. Default:10.
     *             wnd_type: 0..x      // PSD window type: 0=Welch, others not yet supported. Default:0.
     *             wnd_envelope: 0..1  // Use maximum of windows instead of averaging. Default:0.
     *             freq_min: Number    // Minimum frequency in Hz. Default:0 (unlimited)
     *             freq_max: Number    // Maximum frequency in Hz. Default:unlimited
     *             file_format: 0..1   // Output file format: 0=text+binary files, 1=text only. Default:0.
     *             use_headers: 0..1   // Add GNU 'nonuniform' matrix headers. Default:0.
     *             zero_fill: 0..1     // Zero-fill gaps when data missing. 0:No, make sparse, 1:Yes, GNU compatible
     *             gnuplot: 0..1       // 1=Export gnuplot scripts (forces zero_fill=1, use_headers=1). Default=1.
     *             guntemplate: String //
     *         }
     *     }
     *
     *     // Export of Digital Input data
     *     din:
     *     {
     *         din_format: 0..3      // Digital Input data format. 0=Counts/sec, 1=Edges only, 2=Signal+Edges,
     *                               //       3=Signal+Edges also derived from signal. Default:0.
     *     },
     * }
     *
     * // Example of returned object
     * {
     *     status: 0               // operation status, >=0 success, <0 error
     *     job_id: Number          // background job identifier for further querries and control
     * }
     */
    async ExportFileReaderData(reader_id, outdir, subdir, export_cfg, make_unique_dir = true) {
        this.assert_min_version(488);
        return this.call("ExportFileReaderData", {
            "path": outdir,
            "subdir": subdir,
            "unique_subdir": make_unique_dir,
            "reader_id": reader_id,
            "export_cfg": export_cfg
        });
    }

    /**
     * ZEDO RPC call: Export multiple file readers or other item's data
     * @param { String|Array<String> } items - Name or array of names of items to be exported ("_id:NNN" is also accepted as a name)
     * @param { Object } export_cfg - Export configuration. See ExportFileReaderData for more information.
     *                                Additional `allow` and `deny` items are possible when localization
     *                                group is exported. Examples: "l_locevn", "locfilt"
     * @return { Promise<Object> } '.then' returns the response object (check .status to be zero)
     * @public
     * @example
     * zedo.ExportItems(["my_item1", "_id:100212"], export_cfg).then((resp) => { console.log(resp); });
     */
    async ExportItems(items, outdir, subdir, export_cfg, make_unique_dir = true) {
        this.assert_min_version(503);
        return this.call("ExportItems", {
            "path": outdir,
            "subdir": subdir,
            "unique_subdir": make_unique_dir,
            "items": this.get_array_or_item(items, true),
            "export_cfg": export_cfg
        });
    }

    /**
     * ZEDO RPC call: Capture graph pictures currently open in ZDaemon into a specific folder
     * @param { String } outdir - Output directory
     * @param { String } subdir - Subdirectory
     * @param { Boolean } make_unique_dir - If 'subdir' is specified, it will be modified to be unique within outdir
     * @param { Boolean } make_unique_files - Make the files unique if output directory and files already exist
     * @return { Promise<Object> } '.then' returns the response object
     * @public
     * @example
     * zedo.CaptureGraphPictures("C:\\Output").then((resp) => { console.log(resp); });
     */
    async CaptureGraphPictures(outdir, subdir = "graphs", make_unique_dir = true, make_unique_files = true) {
        this.assert_min_version(487);
        return this.call("CaptureGraphPictures", {
            "path": outdir,
            "subdir": subdir,
            "unique_subdir": make_unique_dir,
            "unique_files": make_unique_files
        });
    }

    /**
     * ZEDO RPC call: Wait until File Reader finishes file scanning
     * @param { Number } reader_id - File reader ID received from OpenFileReader
     * @return { Promise<Object> } '.then' returns the response object (check .status to be zero)
     * @public
     * @example
     * zedo.WaitFileReaderScanned(reader_id).then((resp) => { console.log(resp); });
     *
     * // Example of returned object
     * {
     *     status: 0               // operation status, >=0 success, <0 error
     * }
     */
    async WaitFileReaderScanned(reader_id) {
        this.assert_min_version(487);
        return this.call("WaitFileReaderScanned", { "reader_id": reader_id });
    }

    /**
     * ZEDO RPC call: Wait until all specified items and their child items are idle.
     * This is a superset of WaitFileReaderScanned() as multiple items can be specifed.
     * Also, this function does not check only a file reader status, but also other
     * item-specific processing state (e.g. localization group processing, etc.)
     * @param { String|Array<String> } items - Name or array of names of items to be exported ("_id:NNN" is also accepted as a name)
     * @return { Promise<Object> } '.then' returns the response object (check .status to be zero)
     * @public
     * @example
     * zedo.WaitItemsIdle(["my_item", "other_item"]).then((resp) => { console.log(resp); });
     *
     * // Example of returned object
     * {
     *     status: 0               // operation status, >=0 success, <0 error
     * }
     */
    async WaitItemsIdle(items) {
        this.assert_min_version(503);
        return this.call("WaitItemsIdle", {
            "items": this.get_array_or_item(items, true),
        });
    }

    /**
     * ZEDO RPC call: Get status of File Reader Export Job
     * @param { Number } job_id - Export Job ID received from ExportFileReaderData
     * @return { Promise<Object> } '.then' returns the response object (check .status to be zero)
     * @public
     * @example
     * zedo.GetExportJobStatus(job_id).then((resp) => { console.log(resp); });
     *
     * // Example of returned object
     * {
     *     job_id: Number          // current job_id
     *     done: Boolean           // true if export finished
     *     progress: Number        // percentage progress
     *     out_files: Array<String> // list of files generated
     *     errors: Array<String>   // list export errors generated
     *     status: 0               // operation status, >=0 success, <0 error
     * }
     */
    async GetExportJobStatus(job_id) {
        this.assert_min_version(491);
        return this.call("GetExportJobStatus", { "job_id": job_id });
    }

    /**
     * ZEDO RPC call: Abort File Reader Export Job
     * @param { Number } job_id - Export Job ID received from ExportFileReaderData
     * @return { Promise<Object> } '.then' returns the response object (check .status to be zero)
     * @public
     * @example
     * zedo.AbortExportJob(job_id).then((resp) => { console.log(resp); });
     *
     * // Example of returned object
     * {
     *     status: 0               // operation status, >=0 success, <0 error
     * }
     */
    async AbortExportJob(job_id) {
        this.assert_min_version(491);
        return this.call("AbortExportJob", { "job_id": job_id });
    }

    /**
     * ZEDO RPC call: Wait until File Reader Export Job finishes with timeout
     * @param { Number } job_id - Export Job ID received from ExportFileReaderData
     * @return { Promise<Object> } '.then' returns the response object (check .status to be zero)
     * @public
     * @example
     * zedo.WaitBackroundJobFinished(job_id, 10.0).then((resp) => { console.log(resp); });
     *
     * // Example of returned object
     * {
     *     status: 0               // operation status, >=0 success, <0 error
     *     ...                     // if status==0, more info same as from GetExportJobStatus
     * }
     */
    async WaitBackroundJobFinished(job_id, timeout_seconds) {
        this.assert_min_version(491);
        return this.call("WaitBackroundJobFinished", { "job_id": job_id, "timeout": timeout_seconds });
    }

    /**
     * ZEDO RPC call: Start timer to call GetExportJobStatus periodically to monitor the export job
     * @param { Number } job_id - Export Job ID received from ExportFileReaderData
     * @param { Number } period_sec - Status checking period in seconds
     * @param { Number } timeout_sec - Total operation timeout in seconds. Use 0 to disable timeout.
     * @param { Function } proggress_callback - User function called each period to report GetExportJobStatus. Callback may return true to abort waiting.
     * @return { Promise<Object> } Resolved when export is finished, when callback aborts it or when timeout occurs. Rejected in case of error.
     * @public
     * @example
     * zedo.ExportJobProgressMonitor(job_id, 1, 60, monitor_callback).then((resp) => { console.log(resp); });
     *
     * // Returned object is same as GetExportJobStatus, extended by monitor members
     * {
     *     job_id: Number          // current job_id
     *     done: Boolean           // true if export finished
     *     progress: Number        // percentage progress
     *     out_files: Array<String> // list of files generated
     *     errors: Array<String>   // list export errors generated
     *     time_running: Number    // job duration in seconds
     *     time_out: Boolean       // time out state (set to true also in the last callback if timeout occurs)
     *     status: 0               // operation status, >=0 success, <0 error
     * }
     *
     * // Example of a callback function
     * function monitor_callback(stat)
     * {
     *      console.log(`Export job ${jobstate.job_id} progress ${jobstate.progress} (time=${jobstate.time_running})`);
     *      return false; // return true to abort, false to continue
     * }
     */
    async ExportJobProgressMonitor(job_id, period_sec, timeout_sec, proggress_callback) {
        this.assert_min_version(491);

        let basetime = process.uptime();
        let mutex = 0;

        return new Promise((resolve, reject) => {
            let timer = setInterval(() => {
                if (mutex++ > 0) {
                    // console.log("ExportJobProgressMonitor: last GetExportJobStatus call not yet finished, waiting...")
                    mutex--;
                    return;
                }
                this.GetExportJobStatus(job_id)
                    .then((res) => {
                        if (res.status == 0) {
                            let abort = false;

                            res.time_running = process.uptime() - basetime;
                            res.time_out = timeout_sec > 0 && res.time_running > timeout_sec;

                            if (proggress_callback)
                                abort = proggress_callback(res);

                            if (res.done || abort || res.time_out) {
                                clearInterval(timer);
                                resolve(res);
                            }
                            else {
                                // leave the timer running...
                            }
                        }
                        else {
                            clearInterval(timer);
                            reject(`GetExportJobStatus failed while probing job_id=${job_id}`);
                        }
                    })
                    .catch((err) => {
                        clearInterval(timer);
                        reject(`GetExportJobStatus exception while probing job_id=${job_id}: ` + err);
                    })
                    .finally(() => {
                        mutex--;
                    })
            }, /* setInterval: */ period_sec * 1000);
        });
    }

    /**
     * ZEDO RPC call: Determine if item exists and get its descroption
     * @param { String } name - Name of item to be retrieve ("_id:NNN" is also accepted as a name)
     * @param { String } verbosity - Required verbosity of output; examples: "min", "med", "max", "all"
     * @return { Promise<Object> } '.then' returns the response object (check .status to be zero)
     * @public
     * @example
     * zedo.GetItemInfo("my_item").then((resp) => { console.log(resp); });
     */
    async GetItemInfo(name, verbosity = "max") {
        this.assert_min_version(503);
        return this.call("GetItemInfo", {
            "name": name,
            "verbosity": verbosity
        });
    }

    /**
     * ZEDO RPC call: Get sub-items of a specified item
     * @param { String } name - Name of item to be retrieve ("_id:NNN" is also accepted as a name)
     * @return { Promise<Object> } '.then' returns the response object (check .status to be zero)
     * @public
     * @example
     * zedo.GetSubItems("my_item").then((resp) => { console.log(resp); });
     *
     * // Example of returned object
     * {
     *     items: [
     *        { _id: 100100, name: "item1" },
     *        { _id: 100102, name: "item2" },
     *        { _id: 100104, name: "item3" },
     *     ]
     *     status: 0               // operation status, >=0 success, <0 error
     * }
     *
     */
    async GetSubItems(name) {
        this.assert_min_version(503);
        return this.call("GetSubItems", {
            "name": name,
        });
    }

};

ZedoRPC.prototype.LOG_NONE = 0;
ZedoRPC.prototype.LOG_ERR = 1;
ZedoRPC.prototype.LOG_INFO = 2;
ZedoRPC.prototype.LOG_ALL = 3;

ZedoRPC.prototype.STATUS_TO_NAME = {
    // good status values
    "0": "OK",
    "1": "OK_NOCHANGE",

    // errors
    "-1": "NOTFOUND",
    "-2": "BADTYPE",
    "-3": "BADVALUE",
    "-4": "BUSY",
    "-5": "INTERR",
    "-6": "NOTIMPL",
};

module.exports = ZedoRPC;
