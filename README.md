## zedo-rpc - node.js interface to Dakel ZEDO 

Link to [API documentation][1].

### Overview 
The **zedo-rpc** module implements the [ZedoRPC][1] class which can be used to simplify a JSON-RPC access to Dakel ZEDO system for Acoustic Emission measurements. Dakel ZEDO is an advanced AE measurement system scalable from a single channel to hundreds of channels enabling AE signal acquisition and analysis, AE hit detection, AE events localization and more.

Dakel ZEDO is controlled by a software called ZDaemon (ZEDO Diagnostic AE Monitor) which runs on a Windows-based PC with rich graphical interface. The ZDaemon exports many services over a JSON-RPC API which can be accessed from external applications coded in any programming language. 

[1]: ./API.md